S = minetest.get_translator("aes_suggestion_box")

minetest.register_node("aes_suggestion_box:box",{
  description = "Casella dei suggerimenti",
  tiles = {
    "suggestion_box2.png",
    "suggestion_box4.png",
    "suggestion_box5.png",
    "suggestion_box5.png",
    "suggestion_box1.png",
    "suggestion_box3.png",
  },
  wield_scale = 1.0,
  groups = {cracky = 3, oddly_breakable_by_hand = 3},

  paramtype2 = "facedir",

  on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)

    local itemname = itemstack:get_name()

    if itemname ~= "default:book_written" and itemname ~= "default:book" then return end

    local p_name = clicker:get_player_name()

    if itemname == "default:book" then
      itemstack:take_item()
      minetest.chat_send_player(p_name, S("You have mailed an empty book"))
    return itemstack end

    pos.y = pos.y -2

    local node = minetest.get_node(pos)

    if node.name ~= "default:chest" then
      minetest.set_node(pos, {name ="default:chest"})
    end

    local inv = minetest.get_inventory({ type="node", pos=pos })

    if inv:room_for_item("main", itemstack) then

      inv:add_item("main", itemstack)

      -- È messo sia nel return che qua in quanto se lo si toglie e basta dopo non
      --lo si riesce più a mettere nella chest
      itemstack:take_item()

      minetest.chat_send_player(p_name, S("Your suggestion has been mailed"))
    else
      minetest.chat_send_player(p_name, S("There's no more space for other suggestions :("))
    end

    return itemstack

  end,


})
